Feature: Search Nearest Doctors. As person looking for doctor consultation service

@watch
Scenario: Looking for a list of doctors nearest to my current location.
  Given I have browsed to the home page
  When I search for "doctors"
  Then I see list of doctors
  And Should be in sorted inorder of nearest
  And Should click on "Book" button

  Scenario: Looking for a list of doctors nearest to my current location.
    Given I have browsed to the home page
    When I search for "doctors"
    Then I see list of doctors
    And Should be in sorted inorder of nearest
    And Should click on "Book" button
