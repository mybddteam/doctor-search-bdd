/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('doctorListBdd')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
