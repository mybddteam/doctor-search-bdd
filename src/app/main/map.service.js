(function () {
  angular.module('doctorListBdd').factory('MapService', ['$q', '$http', function ($q, $http) {
    var service = {}
    service.init = function (position) {
      console.log(position);
      var lat = 40.7127837;
      var lng = -74.00594130000002;
      var options = {
        center: new google.maps.LatLng(lat, lng),
        zoom:13,
        disableDefaultUI:true
      };
      this.map = new google.maps.Map(
        document.getElementById('map'), options
      );
      this.places = new google.maps.places.PlacesService(this.map);
    };
    service.search = function (text) {
      var d = $q.defer();
      this.places.textSearch({query:text},
        function (resp, status) {
          if(status==='OK'){
            d.resolve(resp);
          } else{
            d.reject(status);
          }
        });
      return d.promise;
    };
    service.addMarker = function (res) {
      if(this.marker) this.marker.setMap(null);
      this.marker = new google.maps.Marker({
        map: this.map,
        position: res.geometry.location,
        animation: google.maps.Animation.DROP
      });
      this.map.setCenter(res.geometry.location);
    };
    return service;
  }]);
})();
