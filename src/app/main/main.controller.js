(function() {
  'use strict';

  angular
    .module('doctorListBdd')
    .controller('MainController', ['MapService', '$scope', '$log', 'searchTerm', MainController]);

  /** @ngInject */
  function MainController(MapService, $scope, $log, searchTerm) {
    var vm = this;
    $scope.searchTerm = searchTerm;
    $scope.places;
    $scope.isPlaceBooking = false;

    $scope.placeBooking = function (item) {
      $scope.isPlaceBooking = true;
      $scope.place = item;
      $scope.place.name= item.name;
      $scope.place.lat = item.geometry.location.lat();
      $scope.place.lng = item.geometry.location.lng();
    };

    $scope.showSearch = function () {
      $scope.isPlaceBooking = false;
      $scope.place = null;
    };
    $scope.search = function () {
      vm.error = false;
      MapService.search($scope.text)
        .then(function (items) {
          angular.forEach(items, function (item) {
            MapService.addMarker(item);
            item.lat = item.geometry.location.lat();
            item.lng = item.geometry.location.lng();
          });
          $scope.places = items;
        })
        .catch(function (status) {
          $scope.error = true;
          $scope.apiStatus = status;
        });
    };

    MapService.init();

  }
})();
