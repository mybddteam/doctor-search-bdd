(function() {
  'use strict';

  angular
    .module('doctorListBdd')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
