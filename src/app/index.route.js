(function() {
  'use strict';

  angular
    .module('doctorListBdd')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/doctors',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        resolve:{searchTerm:[function(){
          return '';
        }]}
      }).state('doctors', {
        url: '/doctors/:searchTerm',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        resolve:{searchTerm:['$stateParams', function($stateParams){
          return $stateParams.searchTerm;
        }]}
      });

    $urlRouterProvider.otherwise('/doctors');
  }

})();


/*
,
resolve:{position:['$q', function($q){
  var d = $q.defer();
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (data, status) {
      if(status==='OK'){
        d.resolve(resp);
      } else{
        d.reject(status);
      }
    });
  } else {
    d.reject('get current location not supported');
  }
  return d.promise;
}]}*/
